FORCE:
build: FORCE
	npm run build
docker: build
	docker build -t registry.gitlab.com/spruett/chordgen-www .
push: docker
	docker push registry.gitlab.com/spruett/chordgen-www
proto: proto/chordgen.proto
	./generate-proto.sh
