FROM nginx:alpine

ADD build/ /usr/share/nginx/html
ADD ops/nginx-server.conf /etc/nginx/conf.d/default.conf
