// package: chordgen
// file: proto/chordgen.proto

import * as jspb from "google-protobuf";

export class Interval extends jspb.Message {
  getRange(): number;
  setRange(value: number): void;

  getSharps(): number;
  setSharps(value: number): void;

  getFlats(): number;
  setFlats(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Interval.AsObject;
  static toObject(includeInstance: boolean, msg: Interval): Interval.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Interval, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Interval;
  static deserializeBinaryFromReader(message: Interval, reader: jspb.BinaryReader): Interval;
}

export namespace Interval {
  export type AsObject = {
    range: number,
    sharps: number,
    flats: number,
  }
}

export class Note extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getType(): NoteType;
  setType(value: NoteType): void;

  hasInterval(): boolean;
  clearInterval(): void;
  getInterval(): Interval | undefined;
  setInterval(value?: Interval): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Note.AsObject;
  static toObject(includeInstance: boolean, msg: Note): Note.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Note, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Note;
  static deserializeBinaryFromReader(message: Note, reader: jspb.BinaryReader): Note;
}

export namespace Note {
  export type AsObject = {
    name: string,
    type: NoteType,
    interval?: Interval.AsObject,
  }
}

export class PlayedNote extends jspb.Message {
  hasNote(): boolean;
  clearNote(): void;
  getNote(): Note | undefined;
  setNote(value?: Note): void;

  hasString(): boolean;
  clearString(): void;
  getString(): GuitarString | undefined;
  setString(value?: GuitarString): void;

  getFret(): number;
  setFret(value: number): void;

  getFinger(): number;
  setFinger(value: number): void;

  getPitch(): number;
  setPitch(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PlayedNote.AsObject;
  static toObject(includeInstance: boolean, msg: PlayedNote): PlayedNote.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PlayedNote, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PlayedNote;
  static deserializeBinaryFromReader(message: PlayedNote, reader: jspb.BinaryReader): PlayedNote;
}

export namespace PlayedNote {
  export type AsObject = {
    note?: Note.AsObject,
    string?: GuitarString.AsObject,
    fret: number,
    finger: number,
    pitch: number,
  }
}

export class Chord extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  clearNotesList(): void;
  getNotesList(): Array<Note>;
  setNotesList(value: Array<Note>): void;
  addNotes(value?: Note, index?: number): Note;

  clearFlavorsList(): void;
  getFlavorsList(): Array<ChordFlavor>;
  setFlavorsList(value: Array<ChordFlavor>): void;
  addFlavors(value: ChordFlavor, index?: number): ChordFlavor;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Chord.AsObject;
  static toObject(includeInstance: boolean, msg: Chord): Chord.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Chord, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Chord;
  static deserializeBinaryFromReader(message: Chord, reader: jspb.BinaryReader): Chord;
}

export namespace Chord {
  export type AsObject = {
    name: string,
    notesList: Array<Note.AsObject>,
    flavorsList: Array<ChordFlavor>,
  }
}

export class GuitarString extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getIndex(): number;
  setIndex(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GuitarString.AsObject;
  static toObject(includeInstance: boolean, msg: GuitarString): GuitarString.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GuitarString, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GuitarString;
  static deserializeBinaryFromReader(message: GuitarString, reader: jspb.BinaryReader): GuitarString;
}

export namespace GuitarString {
  export type AsObject = {
    name: string,
    index: number,
  }
}

export class Tuning extends jspb.Message {
  getId(): TuningId;
  setId(value: TuningId): void;

  getCapo(): number;
  setCapo(value: number): void;

  clearStringsList(): void;
  getStringsList(): Array<GuitarString>;
  setStringsList(value: Array<GuitarString>): void;
  addStrings(value?: GuitarString, index?: number): GuitarString;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Tuning.AsObject;
  static toObject(includeInstance: boolean, msg: Tuning): Tuning.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Tuning, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Tuning;
  static deserializeBinaryFromReader(message: Tuning, reader: jspb.BinaryReader): Tuning;
}

export namespace Tuning {
  export type AsObject = {
    id: TuningId,
    capo: number,
    stringsList: Array<GuitarString.AsObject>,
  }
}

export class Voicing extends jspb.Message {
  hasChord(): boolean;
  clearChord(): void;
  getChord(): Chord | undefined;
  setChord(value?: Chord): void;

  clearNotesList(): void;
  getNotesList(): Array<PlayedNote>;
  setNotesList(value: Array<PlayedNote>): void;
  addNotes(value?: PlayedNote, index?: number): PlayedNote;

  hasTuning(): boolean;
  clearTuning(): void;
  getTuning(): Tuning | undefined;
  setTuning(value?: Tuning): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Voicing.AsObject;
  static toObject(includeInstance: boolean, msg: Voicing): Voicing.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Voicing, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Voicing;
  static deserializeBinaryFromReader(message: Voicing, reader: jspb.BinaryReader): Voicing;
}

export namespace Voicing {
  export type AsObject = {
    chord?: Chord.AsObject,
    notesList: Array<PlayedNote.AsObject>,
    tuning?: Tuning.AsObject,
  }
}

export class ScoringInformation extends jspb.Message {
  getScore(): number;
  setScore(value: number): void;

  getSubscoresMap(): jspb.Map<string, number>;
  clearSubscoresMap(): void;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ScoringInformation.AsObject;
  static toObject(includeInstance: boolean, msg: ScoringInformation): ScoringInformation.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ScoringInformation, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ScoringInformation;
  static deserializeBinaryFromReader(message: ScoringInformation, reader: jspb.BinaryReader): ScoringInformation;
}

export namespace ScoringInformation {
  export type AsObject = {
    score: number,
    subscoresMap: Array<[string, number]>,
  }
}

export class Settings extends jspb.Message {
  getTuning(): TuningId;
  setTuning(value: TuningId): void;

  getCapo(): number;
  setCapo(value: number): void;

  getTargetposition(): number;
  setTargetposition(value: number): void;

  getTargetfret(): number;
  setTargetfret(value: number): void;

  getDisablededup(): boolean;
  setDisablededup(value: boolean): void;

  getDisablebars(): boolean;
  setDisablebars(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Settings.AsObject;
  static toObject(includeInstance: boolean, msg: Settings): Settings.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Settings, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Settings;
  static deserializeBinaryFromReader(message: Settings, reader: jspb.BinaryReader): Settings;
}

export namespace Settings {
  export type AsObject = {
    tuning: TuningId,
    capo: number,
    targetposition: number,
    targetfret: number,
    disablededup: boolean,
    disablebars: boolean,
  }
}

export class NoteList extends jspb.Message {
  clearNotesList(): void;
  getNotesList(): Array<PlayedNote>;
  setNotesList(value: Array<PlayedNote>): void;
  addNotes(value?: PlayedNote, index?: number): PlayedNote;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NoteList.AsObject;
  static toObject(includeInstance: boolean, msg: NoteList): NoteList.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: NoteList, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NoteList;
  static deserializeBinaryFromReader(message: NoteList, reader: jspb.BinaryReader): NoteList;
}

export namespace NoteList {
  export type AsObject = {
    notesList: Array<PlayedNote.AsObject>,
  }
}

export class VoicingSet extends jspb.Message {
  hasChord(): boolean;
  clearChord(): void;
  getChord(): Chord | undefined;
  setChord(value?: Chord): void;

  hasTuning(): boolean;
  clearTuning(): void;
  getTuning(): Tuning | undefined;
  setTuning(value?: Tuning): void;

  clearVoicingsList(): void;
  getVoicingsList(): Array<NoteList>;
  setVoicingsList(value: Array<NoteList>): void;
  addVoicings(value?: NoteList, index?: number): NoteList;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VoicingSet.AsObject;
  static toObject(includeInstance: boolean, msg: VoicingSet): VoicingSet.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: VoicingSet, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VoicingSet;
  static deserializeBinaryFromReader(message: VoicingSet, reader: jspb.BinaryReader): VoicingSet;
}

export namespace VoicingSet {
  export type AsObject = {
    chord?: Chord.AsObject,
    tuning?: Tuning.AsObject,
    voicingsList: Array<NoteList.AsObject>,
  }
}

export class AllChordVoicings extends jspb.Message {
  getVoicingsMap(): jspb.Map<string, VoicingSet>;
  clearVoicingsMap(): void;
  getAliasesMap(): jspb.Map<string, string>;
  clearAliasesMap(): void;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AllChordVoicings.AsObject;
  static toObject(includeInstance: boolean, msg: AllChordVoicings): AllChordVoicings.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AllChordVoicings, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AllChordVoicings;
  static deserializeBinaryFromReader(message: AllChordVoicings, reader: jspb.BinaryReader): AllChordVoicings;
}

export namespace AllChordVoicings {
  export type AsObject = {
    voicingsMap: Array<[string, VoicingSet.AsObject]>,
    aliasesMap: Array<[string, string]>,
  }
}

export class Error extends jspb.Message {
  getMessage(): string;
  setMessage(value: string): void;

  getCode(): ErrorCode;
  setCode(value: ErrorCode): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Error.AsObject;
  static toObject(includeInstance: boolean, msg: Error): Error.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Error, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Error;
  static deserializeBinaryFromReader(message: Error, reader: jspb.BinaryReader): Error;
}

export namespace Error {
  export type AsObject = {
    message: string,
    code: ErrorCode,
  }
}

export enum NoteType {
  ROOT = 0,
  THIRD = 1,
  FIFTH = 2,
  SEVENTH = 3,
  EXTENSION = 4,
  BASS = 5,
  MELODY = 6,
}

export enum ChordFlavor {
  MAJOR = 0,
  MINOR = 1,
  DIMINISHED = 2,
  AUGMENTED = 3,
  DOMINANT = 4,
  MAJOR_SEVEN = 5,
  SIXTH = 6,
  EXTENSIONS = 20,
}

export enum TuningId {
  STANDARD = 0,
  DROP_D = 1,
  UKULELE = 2,
  DOUBLE_DROP_D = 3,
  DADGAD = 4,
  OPEN_D = 5,
  OPEN_E = 6,
  OPEN_G = 7,
  OPEN_A = 8,
}

export enum ErrorCode {
  NONE = 0,
  NOTE_PARSE_INVALID = 1,
  NOTE_PARSE_SHARP_FLAT = 2,
  PARSE_EMPTY = 3,
  CHORD_NAME_INVALID = 4,
  CHORD_SUFFIX_INVALID = 5,
}

