import { isNullOrUndefined } from "util";

export default function nullthrows<T>(val: T | undefined | null): T {
  if (isNullOrUndefined(val)) {
    throw Error("unexpected null or undefined");
  }
  return val;
}