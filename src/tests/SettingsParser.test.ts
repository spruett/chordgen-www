import parseChordAndSettings from "../util/SettingsParser";
import { TuningId } from "../gen/proto/chordgen_pb";

it('parses chord names', () => {
  let chordAndSettings = parseChordAndSettings("C#m");
  expect(chordAndSettings.chordName).toBe("C#m")

  chordAndSettings = parseChordAndSettings("Gb7")
  expect(chordAndSettings.chordName).toBe("Gb7");
});

it('ignores whitespace', () => {
  const chordAndSettings = parseChordAndSettings("C  ");
  expect(chordAndSettings.chordName).toBe("C")
});

it('parses tunings', () => {
  const chordAndSettings = parseChordAndSettings("C on ukulele");
  expect(chordAndSettings.chordName).toBe("C");
  expect(chordAndSettings.settings.getTuning()).toBe(TuningId.UKULELE);
});

it('parses frets and capos', () => {
  let settings = parseChordAndSettings("C on ukulele near fret 5 with capo on 1").settings;
  expect(settings.getTuning()).toBe(TuningId.UKULELE);
  expect(settings.getTargetfret()).toBe(5);
  expect(settings.getCapo()).toBe(1);
});